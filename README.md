# PhamLuan Php nâng cao

***PHP nâng cao***
- Tìm hiểu về magic method trong PHP
- tìm hiểu về autoload trong PHP
- tìm hiểu thuật toán được sử dụng trong một số hàm bult-in trong php: sắp xếp, tìm kiếm, so sánh, thay thế
- tìm hiểu các điểm mới trong phiên bản PHP mới nhất so với các bản còn trong thời gian support
- Thực hiện bởi [Phạm Luận](https://www.facebook.com/phluann/)

***Tham khảo***
- [Viblo: Magic method](https://viblo.asia/p/magic-methods-trong-php-4dbZN7bklYM)
- [Viblo: điểm mới trong PHP8](https://viblo.asia/p/co-gi-moi-o-phien-ban-php-8-4P856q11lY3)
- [Viblo PHP autoloading](https://viblo.asia/p/php-autoloading-psr4-and-composer-V3m5Wy0QZO7)
## Kiến thức nắm được

### Magic method

- magic method là các phương thức đặc biệt để tùy biến các sự kiện trong php
- một magic method có tốc độ chậm hơn các phương thức bình thường
- các hàm magic method được viết trong 1 class cụ thể mà khi ta thao tác với đối tượng của class đó
mà tùy trường hợp các hàm magic method đã khai báo trong class đó sẽ được thực thi
- trong php hiện nay có 15 magic method

***__construct()***
- hàm được gọi khi khởi tạo một đối tượng
- hàm __construct() sẽ được gọi tự động khi khởi tạo một đối tượng, còn gọi là hàm khởi tạo
```
    class Person{
        public $name;
        public function __construct($name)
        {
            $this->name = $name;
            echo "Khoi tao doi tương Person: name = $name <br>";
        }
    }
    $person = new Person("Luan");       //output: Khoi tao doi tương Person: name = Luan
```
***__destruct()***
- được gọi khi đối tượng bị hủy, mặc định là khi kết thúc chương trinh
```
    class Person{
        public $name;
        public function __construct($name)
        {
            $this->name = $name;
            echo "Khoi tao doi tương Person: name = $name <br>";
        }
        public function __destruct()
        {
            echo "Doi tuong $this->name da bi huy <br>";
        }
    }
    $person = new Person("Luan");       //output: Khoi tao doi tương Person: name = Luan
    unset($person);                           //output: Doi tuong Luan da bi huy
    $person1 = new Person("Nam");       // output: Khoi tao doi tương Person: name = Nam
    //output: Doi tuong Nam da bi huy (co output nay vi ket thuc chuong trinh mac dinh goi __destruct()
```
***__set()***
- gọi khi ta truyền dữ liệu vào thuộc tính không tồn tại hoặc thuộc tính private trong đối tượng
```
    class Set{
        private $ho;
        public function __set($name, $value)
        {
            echo "set thuoc tinh $name, co gia tri: $value <br>";
        }
    }
    $set = new Set();
    $set->name = "Luan";   //output: set thuoc tinh name, co gia tri: Luan
    $set->ho = "Pham";      //output: set thuoc tinh ho, co gia tri: Pham
``` 
- Nó truyền dưới dạng key => value. Như ở ví dụ trên, ta set giá trị cho thuộc tính name 
mà không tồn tại trong class. Nó sẽ gọi đến hàm __set() với $key là thuộc tính đã gọi, 
$value là giá trị đã gán.

***__get()***
- gọi khi ta truy cập vào thuộc tính không tồn tại hoặc thuộc tính private trong đối tượng
- tương tự như __set(), __get() xử lí truy cập đối tượng
```
    class Get{
        private $ho;
        public function __get($name)
        {
            echo "thuoc tinh $name khong co trong doi tuong hoac la private <br>";
        }
    }
    $get = new Get();
    $get->ho;       //output: thuoc tinh ho khong co trong doi tuong hoac la private
    $get->ten;      //output: thuoc tinh ten khong co trong doi tuong hoac la private
```
***__isset()***
- được gọi khi chúng ta kiểm tra một thuộc tính không được phép truy cập của một đối tượng
hay kiểm tra một thuộc tính không tồn tại trong đối tượng đó
- __isset() không sử dụng được với thuộc tính tĩnh
```
    class Issets{
        private $ho;
        public function __isset($name)
        {
            echo "Thuoc tinh $name khong ton tai hoac la private <br>";
        }
    }
    $is = new Issets();
    isset($is->name);   //output: Thuoc tinh name khong ton tai hoac la private
    isset($is->ho);     //output: Thuoc tinh ho khong ton tai hoac la private
```

***__unset()***
- được gọi khi hàm unset() được sử dụng trong một thuộc tính không được phép truy cập. Tương tự như hàm isset.
- Khi ta Unset 1 thuộc tính không tồn tại thì method __unset() sẽ được gọi.
```
    class Unsets{
        private $ho;
        public function __unset($name)
        {
            echo "Thuoc tinh: $name khong ton tai hoạc la private <br>";
        }
    }
    $us = new Unsets();
    unset($us->ho);     //output: Thuoc tinh: ho khong ton tai hoạc la private
    unset($us->name);   //output: Thuoc tinh: name khong ton tai hoạc la private
```

***__call()***
- được gọi khi ta gọi một phương thức không được phép truy cập trong phạm vi của một đối tượng
-  __get() và __call() cũng gần giống nhau. Có điều __get() gọi khi không có thuộc tính còn __call() khi không phương thức
```
    class Call{
        public function __call($name, $arguments)
        {
            echo "Phuong thuc: $name khong ton tai<br>";
            print_r($arguments);
        }
    }
    $c = new Call();
    $c->getHo();    //output: Phuong thuc: getHo khong ton tai
    $c->name("luan", "pham");   //output: Phuong thuc: name khong ton tai
                                //Array ( [0] => luan [1] => pham )
```

***__callSattic()***
- Được kích hoạt khi ta gọi một phương thức không được phép truy cập hoặc chưa được định nghĩa  
```
    class CallStatic{
        private static function name(){

        }
        public static function __callStatic($name, $arguments)
        {
            echo "Method: $name khong duoc phep truy cap, có các tham số: ".implode(', ', $arguments)."<br>";
        }
    }
    $callstatic = new CallStatic();
    $callstatic::name("Luan", "pham");  //output: Method: name khong duoc phep truy cap, có các tham số: Luan, pham
    $callstatic::Infor();               //output: Method: Infor khong duoc phep truy cap, có các tham số:
```

***__toString()***
- được gọi khi `echo` một đối tượng, kiểu trả về bắt buộc phải là string
```
    class B{
        public function __toString()
        {
            return "Goi phuong thuc __toString() <br>";
        }
    }
    $b = new B();
    echo $b;    //output: Goi phuong thuc __toString()
```

***__invoke()***
- được gọi khi ta cố gắng sử dụng một đối tượng như một hàm.
```
    class Invoke{
        public function __invoke($name)
        {
            echo "Day la mot doi tuong, khong phai ham, tham so vua truyen: $name";
        }
    }
    $invoke = new Invoke();
    $invoke("input");       //output: Day la mot doi tuong, khong phai ham, tham so vua truyen: input
```

***__sleep()***
- được gọi khi serialize() một đối tượng.
- thông thường khi chúng ta serialize() một đối tượng thì nó sẽ trả về tất cả các thuộc tính của đối tượng đó
, nhưng nếu sử dụng `__sleep()` thì chúng ta có thể quy định các thuộc tính có thể trả về
- hàm `serialize()` mã hóa giá trị biến truyền vào thành một chuỗi đặc biệt, hàm trả về kết quả là chuỗi được mã hóa.
```
    class ConNguoi{
        private $age, $name;
        public function __sleep()
        {
            return array('name');
        }
    }
    echo serialize(new ConNguoi());     //output: O:8:"ConNguoi":1:{s:14:"ConNguoiname";N;}
``` 

***__wakeUp()***
- được gọi khi unserialize() một đối 
```
    class ConNguoi{
        private $age, $name;
        public function __sleep()
        {
            return array('name');
        }
        public function __wakeup()
        {
            echo "<br>unserialize() doi tuong <br>";
        }
    }
    $ser = serialize(new ConNguoi());
    echo $ser;  //output: O:8:"ConNguoi":1:{s:14:"ConNguoiname";N;}
    unserialize($ser);      //output: unserialize() doi tuong
```
***__set_state()***
- được gọi khi chúng ta var_export một đối tượng
- `var_export()`  dùng để in thông tin của biến truyền vào, có cách sử dụng khá giống với hàm `var_dump()`
```
    class SetState{
        private $a, $b;
    }
    $set = new SetState();
    var_export($set);   //output: SetState::__set_state(array( 'a' => NULL, 'b' => NULL, ))
```

***__clone()***
- được sử dụng khi chúng ta clone một đối tượng (sao chép một đối tượng thành một đối tượng hoàn toàn mới
không liên quan đến đối tượng cũ).
```
    class SetState{
        private $a, $b;
        public function __construct($a, $b)
        {
            $this->a = $a;
            $this->b = $b;
        }

        public function __clone()
        {
            echo "<br> __clone() <br> Doi tuong moi duoc clone $this->a, $this->b <br>";
        }
    }
    $set = new SetState("Luan", "Pham");
    $clone = clone($set);       //output: Doi tuong moi duoc clone Luan, Pham
```

***__debugInfor()***
- được gọi khi chúng ta sử dụng hàm `var_dump()`
```
    class DebugInfor{
        public function __debugInfo()
        {
            echo "Su dụng debugInfor():  ";
        }
    }
    $bug = new DebugInfor();
    var_dump($bug);     //output: Su dụng debugInfor(): object(DebugInfor)#10 (0) { }
```

### Autoload trong PHP
- sử dụng feature autoloading để tự động load các class khi chúng được gọi

***spl_autoload_register()***
- function này sẽ nhận tham số là một callback function
- Nếu có nhiều hàm callback autoload, PHP sẽ tạo 1 queue và thực hiện lần lượt theo thứ 
tự hàm callback được định nghĩa trong lời gọi hàm spl_autoload_register() cho đến khi 
nó tìm được class, và nếu sau khi chạy qua tất cả autoload mà không tìm thấy class 
thì sẽ có exception class not found
```
    spl_autoload_register(function ($class){
        include $class. '.php';
    });
    $person = new Person("Luan Pham");
    $person->infor();   //output: Hi Luan Pham
    $student = new Student("12332");
    $student->infor();  //output: Student Id: 12332
```

### Thuật toán sử dụng bên trong các hàm built-in PHP

#### [Sắp xếp](https://gitlab.com/123doc-trainese/phamluan-php-co-ban#s%E1%BA%AFp-x%E1%BA%BFp)

#### [So sánh](https://gitlab.com/123doc-trainese/phamluan-php-co-ban#so-s%C3%A1nh)

#### Tìm kiếm
- Hàm `array_search()`
  - tìm kiếm một giá trị trong mảng và trả về khóa tương ứng với giá trị đầu tiên mà nó tìm thấy
  - cú pháp `array_search(value, array, strict)`
    - `value` giá trị cần tìm kiếm
    - `array` mảng cần tìm kiếm
    - `strict` (tùy chọn, mặc định là false). nếu tham số này được đặt thành true thì hàm này sẽ tìm kiếm các phần tử giống cả về kiểu dữ liệu trong mảng
 ```
    $a=array("a"=>"5","b"=>5,"c"=>"5");
    echo array_search(5,$a)."<br>";     //output: a
    echo array_search(5,$a, true)."<br>";   //output: b
``` 
- Hàm `array_key_exists()`
  - kiểm tra một mảng để tìm một khóa được chỉ định và trả về true nếu khóa tồn tại và false nếu khóa không tồn tại.
  - cú pháp `array_key_exists(key, array)`
    - `key` khóa cần tìm kiếm
    - `array` mảng ban đầu
  ```
    $a=array("Volvo"=>"XC90","BMW"=>"X5");
    if(array_key_exists("Volvo", $a)){
        echo "Co khoa Volvo <br>";
    }else{
        echo "Khong co khoa Volvo <br>";
    }   //output: Co khoa Volvo
  ```
  
- Hàm `in_array()`
  - tìm kiếm một giá trị cụ thể trong mảng
  - trả về true nếu tìm thấy và false nếu không tìm thấy
  - cú pháp `in_array(search, array, type)`
    - `search` giá trị cần tìm kiếm
    - `array` mảng cần tìm kiếm
    - `type` (tùy chọn, mặc định là false), nếu giá trị này là true thì hàm sẽ tìm kiếm theo kiểu dữ liệu cụ thể trong mảng
    ```
    $people = array("Peter", "Joe", "Glenn", "Cleveland", 23);

    if (in_array("23", $people, TRUE)){
        echo "Tim thay gia tri 23 kieu string <br>";
    } else {
        echo "Khong tim thay gia tri 23 kieu string <br>";
    }   //output: Khong tim thay gia tri 23 kieu string
    if (in_array("Glenn",$people, TRUE)) {
        echo "Tim thay gia tri Gleen <br>";
    } else {
        echo "Khong tim thay gia tri Glenn <br>";
    }   //output: Tim thay gia tri Gleen

    if (in_array(23,$people, TRUE)) {
        echo "Tim thay gia tri 23 kieu int <br>";
    } else {
        echo "Khong tim thay gia tri 23 kieu int <br>";
    }   //output: Tim thay gia tri 23 kieu int
    ``` 
    
#### Thay thế
- Hàm `array_replace()`
  - Hàm array_replace () thay thế các giá trị của mảng đầu tiên bằng các giá trị từ các mảng sau.
  - Nếu một khóa từ array1 tồn tại trong array2, các giá trị từ array1 sẽ được thay thế bằng các giá trị từ array2. Nếu khóa chỉ tồn tại trong array1, nó sẽ được giữ nguyên như cũ 
  - Nếu một khóa tồn tại trong array2 và không tồn tại trong array1, nó sẽ được tạo trong array1 
  - Nếu nhiều mảng được sử dụng, các giá trị từ các mảng sau sẽ ghi đè lên các mảng trước đó
  - Cú pháp `array_replace(array1, array2, array3, ...)`
```
    $a1=array("a"=>"red","b"=>"green");
    $a2=array("a"=>"orange","burgundy");
    print_r(array_replace($a1,$a2));    //output: Array ( [a] => orange [b] => green [0] => burgundy )
```
- Hàm `array_splice()`
  - Hàm array_splice () xóa các phần tử đã chọn khỏi một mảng và thay thế nó bằng các phần tử mới.
  - Hàm cũng trả về một mảng với các phần tử đã bị loại bỏ.
  - cú pháp `array_splice(array, start, length, array)`
    - `array` mảng ban đầu
    - `start` giá trị số, nơi bắt đầu xóa các phần tử, 0 = phần tử đầu tiên, nếu là số âm sẽ tính ngược lại từ cuối mảng
    - `length` (tùy chọn), giá trị số, Chỉ định số lượng phần tử sẽ bị xóa và cả độ dài của mảng được trả về. 
    Nếu giá trị này được đặt thành một số âm, hàm sẽ dừng ở phần tử cuối cùng đó. Nếu giá trị này không được đặt, 
    hàm sẽ xóa tất cả các phần tử, bắt đầu từ vị trí được đặt bởi tham số bắt đầu.
    - `array` (tùy chọn) Chỉ định một mảng với các phần tử sẽ được chèn vào mảng ban đầu.
    Nếu nó chỉ là một phần tử, nó có thể là một chuỗi và không nhất thiết phải là một mảng.
  ```
    $a1=array("a"=>"red","b"=>"green","c"=>"blue","d"=>"yellow");
    $a2=array("a"=>"purple","b"=>"orange");
    print_r(array_splice($a1,0,2,$a2));     //output: Array ( [a] => red [b] => green )
    echo "<br> mang sau khi thay the: <br>";
    print_r($a1);  // output: Array ( [0] => purple [1] => orange [c] => blue [d] => yellow )
  ```
  
- Hàm `str_ireplace()`, `str_replace()`
  - Hàm str_ireplace () thay thế một số ký tự bằng một số ký tự khác trong một chuỗi, không phân biệt chữ hoa chữ thường
  - Hàm str_replace () thay thế một số ký tự bằng một số ký tự khác trong một chuỗi, phân biệt chữ hoa chữ thường
  - cú pháp `str_ireplace(find,replace,string,count)`, `str_replace(find,replace,string,count)`

```
    echo "<br> str_replace(): phân biệt hoa thường <br>";
    $str = "Hello world";
    echo str_replace("World","Luan Pham","Hello world!");   //output: Hello world!
    echo "<br> str_ireplace(): không phân biệt hoa thường<br>";
    echo str_ireplace("World","Luan Pham","Hello world!");  //output: Hello Luan Pham!  
```

- Hàm `substr_replace()`
  - thay thế một phần của chuỗi bằng một chuỗi khác
  - cú pháp `substr_replace(string,replacement,start,length)`
    - `string` chuỗi ban đầu
    - `replacement` chuỗi thay thế
    - `start` vị trí bắt đầu thay thế
      - một số `>0` bắt đầu từ vị trí chỉ định tính từ đầu chuỗi
      - một số `<0` bắt đầu từ vị trí chỉ định tính từ cuối chuỗi
      - `0` bắt đầu từ kí tự đầu tiên trong chuỗi
    - `length` chỉ định số lượng kí tự được thay thế (tùy chọn, mặc định là bằng độ dài chuỗi)
      - một số `>0` số lượng kí tự được thay thế
      - một số `<0` số kí tự không bị thay thế tính từ cuối chuỗi
      - `0` chèn vào chuỗi cũ mà không thay thế
  ```
    $str = "Pham Pham Pham";
    echo "thay thế chuỗi: <br>";
    echo substr_replace($str, "Van Luan", 5)."<br>";   //output: Pham Van Luan
    echo substr_replace($str, "Van Luan", 5, 2)."<br>";
    //output: Pham Van Luanam Pham  (vì chỉ thay thế 2 kí tự là Ph từ vị trí số 5)
    echo substr_replace($str, "Van Luan", 5, -2)."<br>";
    //output: Pham Van Luanam   (vì để lại 2 kí tự cuối cùng là am)
    echo substr_replace($str, "Van Luan ", -4, 0)."<br>";
    //output: Pham Pham Van Luan Pham   (chèn vào chuỗi tại vị trí 4 tính từ cuối chuỗi)
  ```

## Điểm mới trong PHP 8

***Named argument***
- sẽ cho phép truyền biến số vào trong function dựa theo tên của tham số đó thay vì vị trí của tham số đó
- giúp việc định nghĩa function sẽ linh hoạt hơn, không phụ thuộc vào thứ tự và cho phép bỏ qua các param có giá trị mặc định
```
  // Su dung positional arguments:
  array_fill(0, 100, 50);
 
  // Su dung named arguments:
  array_fill(start_index: 0, num: 100, value: 50);
  //hoac
  array_fill(value: 50, num: 100, start_index: 0);
```
***Attributes***
- Attributes là tính năng mới cho phép chúng ta bổ sung thông tin về cấu trúc code, thông tin về metadata trong code như: class, method, function, params
```
  // PHP 7
  class PostsController
  {
      /**
       * @Route("/api/posts/{id}", methods={"GET"})
       */
      public function get($id) { /* ... */ }
  }
  
  // PHP 8
  class PostsController
  {
      #[Route("/api/posts/{id}", methods: ["GET"])]
      public function get($id) { /* ... */ }
  }
```
***Constructor property promotion***
- Hiện tại để viết một object đơn giản chúng ta cần viết kha khá số lượng code, việc lặp lại các dòng code định nghĩa thuộc tính khiến code trở nên rườm rà hơn
- Với PHP 8, hàm __construct đã có thể nhận vào các thuộc tính được định nghĩa cụ thể,
PHP sẽ thông dịch để tạo ra cả thuộc tính của object, tham số của hàm construct và đồng 
thời gán giá trị cho thuộc tính đó luôn.

```
  //PHP8
  class Point {
  public function __construct(
    public float $x = 0.0,
    public float $y = 0.0,
    public float $z = 0.0,
  ) {}
  }
  
  //PHP7
  class Point {
    public float $x;
    public float $y;
    public float $z;
 
    public function __construct(
        float $x = 0.0,
        float $y = 0.0,
        float $z = 0.0,
    ) {
        $this->x = $x;
        $this->y = $y;
        $this->z = $z;
    }
}
```

***Match expression***
- match gần giống với switch
- match có thể chứa kết quả trong biến hoặc trả về trực tiếp
- match không cần sử dụng break để dừng, nó sẽ trả về kết quả với lần match đầu tiên
- match thực hiện một so sánh chính xác tới kiểu dữ liệu (so sánh ===)
```
  //PHP 7
  switch (8.0) {
    case '8.0':
      $result = "string";
      break;
    case 8.0:
      $result = "integer";
      break;
  }
  echo $result;     //output: string
  
  PHP 8
  echo match (8.0) {
    '8.0' => "string",
     8.0 => "integer",
  };  //output: integer
```
***Nullsafe operator***
- Toán tử nullsafe giúp chúng ta đơn giản hơn trong việc xử lý giá trị nếu object đang truy cập null
- Trong phiên bản PHP 7, chúng ta phải kiểm tra giá trị có null hay không rồi mới tiếp tục truy cập tiếp
```
  //PHP 7
  $user =  null;
  if ($user !== null) {
    $address = $user->address;
    if ($address !== null) {
      $city = $address->getCity();
      if ($city !== null) {
        $country = $city->country;
      }
    }
  }
  
  //PHP 8
  $country = $user?->address?->getCity()?->country;
```

