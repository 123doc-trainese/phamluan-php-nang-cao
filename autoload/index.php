<?php
    spl_autoload_register(function ($class){
        include $class. '.php';
    });
    $person = new Person("Luan Pham");
    $person->infor();   //output: Hi Luan Pham
    $student = new Student("12332");
    $student->infor();  //output: Student Id: 12332

?>