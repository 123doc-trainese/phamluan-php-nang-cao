<?php
//  array_search()
    echo "array_search() <br>";
    $a=array("a"=>"5","b"=>5,"c"=>"5");
    echo array_search(5,$a)."<br>";     //output: a
    echo array_search(5,$a, true)."<br>";   //output: b

//  array_key_exists()
    echo "array_key_exists() <br>";
    $a=array("Volvo"=>"XC90","BMW"=>"X5");
    if(array_key_exists("Volvo", $a)){
        echo "Co khoa Volvo <br>";
    }else{
        echo "Khong co khoa Volvo <br>";
    }   //output: Co khoa Volvo

//  in_array()
    echo "in_array() <br>";
    $people = array("Peter", "Joe", "Glenn", "Cleveland", 23);

    if (in_array("23", $people, TRUE)){
        echo "Tim thay gia tri 23 kieu string <br>";
    } else {
        echo "Khong tim thay gia tri 23 kieu string <br>";
    }   //output: Khong tim thay gia tri 23 kieu string
    if (in_array("Glenn",$people, TRUE)) {
        echo "Tim thay gia tri Gleen <br>";
    } else {
        echo "Khong tim thay gia tri Glenn <br>";
    }   //output: Tim thay gia tri Gleen

    if (in_array(23,$people, TRUE)) {
        echo "Tim thay gia tri 23 kieu int <br>";
    } else {
        echo "Khong tim thay gia tri 23 kieu int <br>";
    }   //output: Tim thay gia tri 23 kieu int

//  array_replace()
    echo "array_replace(): <br>";
    $a1=array("a"=>"red","b"=>"green");
    $a2=array("a"=>"orange","burgundy");
    print_r(array_replace($a1,$a2));    //output: Array ( [a] => orange [b] => green [0] => burgundy )

//  array_plice()
    echo "<br>array_splice(): <br>";
    $a1=array("a"=>"red","b"=>"green","c"=>"blue","d"=>"yellow");
    $a2=array("a"=>"purple","b"=>"orange");
    print_r(array_splice($a1,0,2,$a2));     //output: Array ( [a] => red [b] => green )
    echo "<br> mang sau khi thay the: <br>";
    print_r($a1);  // output: Array ( [0] => purple [1] => orange [c] => blue [d] => yellow )

//  str_replace()
    echo "<br> str_replace(): phân biệt hoa thường <br>";
    $str = "Hello world";
    echo str_replace("World","Luan Pham","Hello world!");   //output: Hello world!
    echo "<br> str_ireplace(): không phân biệt hoa thường<br>";
    echo str_ireplace("World","Luan Pham","Hello world!");  //output: Hello Luan Pham!
?>