<?php
//  __construct()
    class Person{
        public $name;
        public function __construct($name)
        {
            $this->name = $name;
            echo "Khoi tao doi tương Person: name = $name <br>";
        }
        public function __destruct()
        {
            echo "Doi tuong $this->name da bi huy <br>";
        }
    }
    $person = new Person("Luan");       //output: Khoi tao doi tương Person: name = Luan
    unset($person);                           //output: Doi tuong Luan da bi huy
    //$person1 = new Person("Nam");       // output: Khoi tao doi tương Person: name = Nam
    //output: Doi tuong Nam da bi huy

// __set()
    class Set{
        private $ho;
        public function __set($name, $value)
        {
            echo "set thuoc tinh $name, co gia tri: $value <br>";
        }
    }
    $set = new Set();
    $set->name = "Luan";   //output: set thuoc tinh name, co gia tri: Luan
    $set->ho = "Pham";      //output: set thuoc tinh ho, co gia tri: Pham

//  __get()
    class Get{
        private $ho;
        public function __get($name)
        {
            echo "thuoc tinh $name khong co trong doi tuong hoac la private <br>";
        }
    }
    $get = new Get();
    $get->ho;       //output: thuoc tinh ho khong co trong doi tuong hoac la private
    $get->ten;      //output: thuoc tinh ten khong co trong doi tuong hoac la private

//  __isset()
    class Issets{
        private $ho;
        public function __isset($name)
        {
            echo "Thuoc tinh $name khong ton tai hoac la private <br>";
        }
    }
    $is = new Issets();
    isset($is->name);   //output: Thuoc tinh name khong ton tai hoac la private
    isset($is->ho);     //output: Thuoc tinh ho khong ton tai hoac la private

//  __unset()
    echo "__unset() <br>";
    class Unsets{
        private $ho;
        public function __unset($name)
        {
            echo "Thuoc tinh: $name khong ton tai hoạc la private <br>";
        }
    }
    $us = new Unsets();
    unset($us->ho);     //output: Thuoc tinh: ho khong ton tai hoạc la private
    unset($us->name);   //output: Thuoc tinh: name khong ton tai hoạc la private

//  __call()
    echo "__call(): <br>";
    class Call{
        public function __call($name, $arguments)
        {
            echo "Phuong thuc: $name khong ton tai<br>";
            print_r($arguments);
        }
    }
    $c = new Call();
    $c->getHo();    //output: Phuong thuc: getHo khong ton tai
    $c->name("luan", "pham");   //output: Phuong thuc: name khong ton tai
                                //Array ( [0] => luan [1] => pham )

//  __callStatic()
    echo "<br>__callstatic(): <br>";
    class CallStatic{
        private static function name(){

        }
        public static function __callStatic($name, $arguments)
        {
            echo "Method: $name khong duoc phep truy cap, có các tham số: ".implode(', ', $arguments)."<br>";
        }
    }
    $callstatic = new CallStatic();
    $callstatic::name("Luan", "pham");  //output: Method: name khong duoc phep truy cap, có các tham số: Luan, pham
    $callstatic::Infor();               //output: Method: Infor khong duoc phep truy cap, có các tham số:

//  __toString()
    echo "<br> __toString(): <br>";
    class B{
        public function __toString()
        {
            return "Goi phuong thuc __toString() <br>";
        }
    }
    $b = new B();
    echo $b;    //output: Goi phuong thuc __toString()

//  __invoke()
    echo "<br> __invoke(): <br>";
    class Invoke{
        public function __invoke($name)
        {
            echo "Day la mot doi tuong, khong phai ham, tham vua truyen: $name <br>";
        }
    }
    $invoke = new Invoke();
    $invoke("input");       //output: Day la mot doi tuong, khong phai ham, tham vua truyen: input

//  __sleep(), __wakeup()
    echo "<br> __sleep(): <br>";
    class ConNguoi{
        private $age, $name;
        public function __sleep()
        {
            return array('name');
        }
        public function __wakeup()
        {
            echo "<br>unserialize() doi tuong <br>";
        }
    }
    $ser = serialize(new ConNguoi());
    echo $ser;  //output: O:8:"ConNguoi":1:{s:14:"ConNguoiname";N;}
    unserialize($ser);      //output: unserialize() doi tuong

//  set_state()
    echo "<br> __set_state(): <br>";
    class SetState{
        private $a, $b;
        public function __construct($a, $b)
        {
            $this->a = $a;
            $this->b = $b;
        }

        public function __clone()
        {
            echo "<br> __clone() <br> Doi tuong moi duoc clone $this->a, $this->b <br>";
        }
    }
    $set = new SetState("Luan", "Pham");
    var_export($set);   //output: SetState::__set_state(array( 'a' => NULL, 'b' => NULL, ))
    $clone = clone($set);       //output: Doi tuong moi duoc clone Luan, Pham

//  __debufInfor()
    echo "<br> __debugInfor(): <br>";
    class DebugInfor{
        public function __debugInfo()
        {
            echo "Su dụng debugInfor():  ";
        }
    }
    $bug = new DebugInfor();
    var_dump($bug);     //output: Su dụng debugInfor(): object(DebugInfor)#10 (0) { }
?>